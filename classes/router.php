<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 17.10.2020
 * Time: 15:28
 */

class Router
{
    /*
    ** Класс маршрутизации
    */

    public static function buildRoute() {

        /*Контроллер и action по умолчанию*/
        $controllerName = "IndexController";
        $modelName = "IndexModel";
        $action = "index";

        $route = explode("/", $_SERVER['REQUEST_URI']);

        /*Определяем контроллер*/
        if($route[1] != '') {
            $controllerName = ucfirst($route[1]. "Controller");
            $modelName = ucfirst($route[1]. "Model");
        }


        //require_once MVC_PATH . $controllerName . ".php"; //IndexController.php
        //require_once MVC_PATH . $modelName . ".php"; //IndexModel.php

        if(isset($route[2]) && $route[2] !='') {
            $action = $route[2];
        }

        $controller = new $controllerName();
        $controller->$action(); // $controller->index();

    }

    public function errorPage()
    {

    }
}