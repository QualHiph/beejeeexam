<?php
/**
 * Класс представления
 * Date: 17.10.2020
 * Time: 15:44
 */

class View
{
    public function render($tpl, $pageData) {
        include ROOT. $tpl;
    }
}