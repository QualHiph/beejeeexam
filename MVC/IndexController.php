<?php
/**
 * Страница индекс
 */

class IndexController extends Controller {

    private $pageTpl = '/tpl/main.php';


    public function __construct() {
        $this->model = new IndexModel();
        $this->view = new View();
    }


    public function index() {
        $this->pageData['title'] = "Список задач";
        $this->view->render($this->pageTpl, $this->pageData);
    }



}