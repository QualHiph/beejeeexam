<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 25.10.2020
 * Time: 15:10
 */

class AuthController extends Controller {

    private $pageTpl = '/tpl/main.php';


    public function __construct() {
        $this->model = new UserModel();
        $this->view = new View();
    }


    public function index() {
        $this->pageData['title'] = "Вход в личный кабинет";
        $this->view->render($this->pageTpl, $this->pageData);
    }



}