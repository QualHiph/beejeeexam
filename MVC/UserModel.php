<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 24.10.2020
 * Time: 20:26
 */
class UserModel extends Model
{
    public function getUser($cred, $pass) {
        $qr = $this->db->prepare("SELECT name, role FROM `namenrole` WHERE cred = ? and pass= ?");
        $qr->bindParam(1, $cred);
        $qr->bindParam(2, $pass);

        $qr->execute($cred, $pass);
        $qr->fetch();
        if (!empty($qr)) {
            if (!empty($qr['name']) && !empty($qr['role'])) {
                return array($qr['name'], $qr['role']);
            }
        }
    }
}