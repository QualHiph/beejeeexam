<?php

define("ROOT", "W:/domains/beejeeexam");
define("MVC_PATH", ROOT. "/MVC/");
define("MODEL_PATH", ROOT. "/models/");
define("VIEW_PATH", ROOT. "/views/");
define("STYLE_PATH", "style/");

function Autoloader($className) {
    // Путь до файла
    //$fileName = __DIR__ . CONTROLLER_PATH . $className . '.php';
    $classFileName = $className . '.php';
    // Если файл существует, то вызываем require и говорим spl-автозагрузчику, что все ок
    for ($folder = 0; $folder < 2; $folder++) {
        switch ($folder) {
            case 0:
                $fileName = ROOT . "/classes/" . $classFileName;
                break;
            case 1:
                $fileName = ROOT . "/MVC/" . $classFileName;
                break;
        }

        if (file_exists($fileName)) {
            require_once $fileName;
            return true;
        }
    }
    // Все не ок. Мы не знаем, что это за файл
    return false;
}

// Регистрируем нашу функцию автозагрузки
spl_autoload_register('Autoloader');

Router::buildRoute();